package UD21.JunitGeometria;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import UD21.JunitGeometriaDto.Geometria;
import junit.framework.TestCase;



/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
	

{
    /**
     * Rigorous Test :-)
     */
	Geometria cuadrado = new Geometria(1);
	Geometria Circulo = new Geometria(2);
	Geometria Triangulo = new Geometria(3);
	Geometria Cuadrado = new Geometria(4);
	Geometria Rectangulo = new Geometria(5);
	Geometria Pentagono = new Geometria(6);
	Geometria Rombo = new Geometria(7);
	Geometria Romboide = new Geometria(8);
	Geometria Trapezoide = new Geometria(9);
	Geometria defaul = new Geometria();
	
	 
	
	@Test
	public void testAreaCuadrado(){
		int resultado = Geometria.areacuadrado(2);
		int esperado = 4;
		assertEquals(resultado,esperado);
		
		}
	
	public void testAreaCirculo() {
		double resultado = Geometria.areaCirculo(2);
		double esperado = 12;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	
	public void testAreaTriangulo() {
		double resultado = Geometria.areatriangulo(4,2);
		double esperado = 4;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	public void testAreaRectangulo() {
		double resultado = Geometria.arearectangulo(4,2);
		double esperado = 8;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	public void testAreaPentagono() {
		double resultado = Geometria.areapentagono(4,2);
		double esperado = 4;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	public void testAreaRombo() {
		double resultado = Geometria.arearombo(4,2);
		double esperado = 4;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	public void testAreaRomboide() {
		double resultado = Geometria.arearomboide(4,2);
		double esperado = 8;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	public void testAreaTrapecio() {
		double resultado = Geometria.areatrapecio(4,2,2);  
		double esperado = 6;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
	
	
	public void testGetId() {
		assertEquals(1, cuadrado.getId());
	}
	public void testDefault() {
		assertEquals(9, defaul.getId());
	}
	
	public void testToString() {
		assertEquals("Geometria [id=1, nom=cuadrado, area=0.0]", cuadrado.toString());
	}
	public void testGetArea() {
		cuadrado.setArea(2);
		assertEquals(2, cuadrado.getArea(), 1);
	}
	public void testSetNom() {
		cuadrado.setNom("triangulo");
		assertEquals("triangulo",cuadrado.getNom());
	}
	public void testSetId() {
		cuadrado.setId(2);
		assertEquals(2, cuadrado.getId());
	}
	
	
	
	
}
